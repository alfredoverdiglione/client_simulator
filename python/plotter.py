##Importing modules to plot datas
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import glob
matplotlib.style.use('ggplot')


dataFileFolder = '../datas/'

fileName = glob.glob(dataFileFolder+"*.csv")	
for i in range(0,len(fileName)):
	## Now we import the data collection created from the main program
	my_file = pd.read_csv(fileName[i])
	## Try to understand if read went good
	print("Plotting: "+fileName[i])
	my_file.plot(x='Time')
plt.show()
