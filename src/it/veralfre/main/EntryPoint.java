package it.veralfre.main;


import it.veralfre.Controller.DefaultController;
import it.veralfre.View.DefaultView;
public class EntryPoint {
	
	public static void main(String args[]){
		/*Logger l = new TextLogger();
		Logger l2 = new CSVLogger("logs.csv");
		Pooler testPooler = new Pooler(10,Mode.Exponential,
				"it.veralfre.simulatedClients.MockSimulatedClient",10,2,10000);
		testPooler.addLogger(l);
		testPooler.addLogger(l2);
		testPooler.StartPooling();*/
		
		DefaultController c = new DefaultController();
		DefaultView v = new DefaultView(c);
		v.setVisible(true);
	}
}