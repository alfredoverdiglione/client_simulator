package it.veralfre.interfaces;

public interface Logger {
	public void LogData(int value);
	public void LogData(String value);
	public void InformationalMessage(String value);
}
