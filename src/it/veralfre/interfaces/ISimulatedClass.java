package it.veralfre.interfaces;

public abstract class ISimulatedClass extends Thread {
	protected String instanceId;
	
	protected ISimulatedClass(){
		super();
	}
	
	public boolean setId(String id){
		if(id!=null && id.length()>0)
		this.instanceId = id;
		else return false;
		return true;
	}
	
}
