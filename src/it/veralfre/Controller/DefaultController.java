package it.veralfre.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.veralfre.clientSimulator.Mode;
import it.veralfre.clientSimulator.Pooler;
import it.veralfre.clientSimulator.loggers.CSVLogger;

public class DefaultController {
	private Pooler thePooler;
	
	
	
	public Pooler getPooler(){
		return thePooler;
	}
	
	public boolean setUpPooler(int upperBound, Mode mode, String concreteClassName,
			int windowSize,int increaseBase, int timeSpan){
		if(upperBound < 0 || mode == null || concreteClassName.length() <1 ||
				windowSize <0 || increaseBase < 1 || timeSpan <0)
			throw new IllegalArgumentException("At least one of the initialization parameter is not valid!");
		thePooler = new Pooler(upperBound, mode, concreteClassName, 
				windowSize, increaseBase, timeSpan);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.ITALIAN);
		
		thePooler.addLogger(new CSVLogger("LOG-"+formatter.format(new Date())+".csv"));
		return true;
	}
	
	public boolean startPooler(){
		thePooler.StartPooling();
		return true;
	}
}
