package it.veralfre.View;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.veralfre.Controller.DefaultController;
import it.veralfre.clientSimulator.Mode;
import it.veralfre.interfaces.Logger;

import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class DefaultView extends JFrame implements ActionListener,Logger {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultController controller;
	private JButton btnStart;
	private JTextArea textArea;
	private JTextArea txtrWindowsize;
	private JTextArea concreteClassName;
	private JTextArea txtrIncreasebase;
	private JTextArea txtrTimespan;
	private JList<Mode> modeListBox;
	private JTextPane logTextPanel;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_2;
	/**
	 * Create the frame.
	 */
	public DefaultView(DefaultController controller) {
		if(controller!=null)
			this.controller = controller;
		
			
		setTitle("Client Simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 582, 674);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnStart = new JButton("Start");
		btnStart.setBounds(356, 36, 89, 23);
		contentPane.add(btnStart);
		btnStart.addActionListener(this);
		
		textArea = new JTextArea();
		textArea.setBounds(10, 35, 163, 23);
		contentPane.add(textArea);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(183, 35, 163, 23);
		contentPane.add(scrollPane_2);
		
		concreteClassName = new JTextArea();
		concreteClassName.setLineWrap(true);
		scrollPane_2.setViewportView(concreteClassName);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 300, 546, 324);
		contentPane.add(scrollPane);
		
		logTextPanel = new JTextPane();
		scrollPane.setViewportView(logTextPanel);
		logTextPanel.setEditable(false);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 115, 163, 100);
		contentPane.add(scrollPane_1);
		
		modeListBox = new JList<Mode>();
		
		modeListBox.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				refreshOptionPanel();
			}

			
		});
		scrollPane_1.setViewportView(modeListBox);
		modeListBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modeListBox.setListData(Mode.values());
		
		JLabel lblUpperBound = new JLabel("Upper Bound:");
		lblUpperBound.setBounds(10, 11, 163, 14);
		contentPane.add(lblUpperBound);
		
		JLabel lblConcreteClassName = new JLabel("Concrete Class Name:");
		lblConcreteClassName.setBounds(183, 11, 163, 14);
		contentPane.add(lblConcreteClassName);
		
		JLabel lblDebug = new JLabel("Debug:");
		lblDebug.setBounds(10, 275, 163, 14);
		contentPane.add(lblDebug);
		
		JLabel lblSpawnMode = new JLabel("Spawn Mode:");
		lblSpawnMode.setBounds(10, 92, 163, 14);
		contentPane.add(lblSpawnMode);
		
		JPanel panel = new JPanel();
		panel.setBounds(183, 115, 349, 100);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblIncreaseBase = new JLabel("Increase Base:");
		lblIncreaseBase.setBounds(10, 10, 113, 14);
		panel.add(lblIncreaseBase);
		
		txtrTimespan = new JTextArea();
		txtrTimespan.setEnabled(false);
		txtrTimespan.setBounds(132, 50, 207, 22);
		panel.add(txtrTimespan);
		
		txtrIncreasebase = new JTextArea();
		txtrIncreasebase.setEnabled(false);
		txtrIncreasebase.setBounds(133, 5, 206, 22);
		panel.add(txtrIncreasebase);
		
		JLabel lblWindowSize = new JLabel("Window Size:");
		lblWindowSize.setBounds(10, 35, 114, 14);
		panel.add(lblWindowSize);
		
		txtrWindowsize = new JTextArea();
		txtrWindowsize.setEnabled(false);
		txtrWindowsize.setBounds(132, 30, 207, 17);
		panel.add(txtrWindowsize);
		
		JLabel lblTimeSpan = new JLabel("Time Span (in seconds):");
		lblTimeSpan.setBounds(10, 55, 114, 14);
		panel.add(lblTimeSpan);
	}

	private void refreshOptionPanel() {
		disableAll();
		txtrTimespan.setEnabled(true);
		switch(modeListBox.getSelectedValue()){
			case Exponential: 
				txtrIncreasebase.setEnabled(true);
				break;
			case Linear:
				break;
			case Windowed:
				txtrWindowsize.setEnabled(true);
				break;
		}
		
	}
	
	private void disableAll(){
		/**We disable all and refresh the default values*/
		
		txtrIncreasebase.setEnabled(false);
		txtrIncreasebase.setText("2");
				
		txtrWindowsize.setEnabled(false);
		txtrWindowsize.setText("10");
		
		txtrTimespan.setEnabled(false);
		txtrTimespan.setText("5");
		
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(modeListBox.getSelectedIndex() >= 0){
		controller.setUpPooler(Integer.parseInt(textArea.getText()) ,(Mode)(modeListBox.getSelectedValue()) , 
				concreteClassName.getText(), Integer.parseInt(txtrIncreasebase.getText()),
				Integer.parseInt(txtrWindowsize.getText()),Integer.parseInt(txtrTimespan.getText())*1000);
		controller.getPooler().addLogger(this);

		controller.startPooler();
		}
		
	}

	@Override
	public void LogData(int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void LogData(String value) {
		
	}

	@Override
	public void InformationalMessage(String value) {
		String oldText = this.logTextPanel.getText();
		this.logTextPanel.setText(oldText+"\n"+value);
	}
}
