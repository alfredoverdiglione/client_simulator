package it.veralfre.clientSimulator;
import java.util.ArrayList;
import java.util.List;

import it.veralfre.interfaces.ISimulatedClass;
import it.veralfre.interfaces.Logger;

public class Pooler {
	
	private int upperBound;
	private Mode growthMode;
	private String concreteClassName;
	private int windowSize;
	private int increaseBase;
	private int timespan;
	
	private List<Logger> loggers;

	public Pooler(int upperBound, Mode growthMode, String concreteClassName, int windowSize, int increaseBase,
			int timespan) {
		super();
		this.upperBound = upperBound;
		this.growthMode = growthMode;
		this.concreteClassName = concreteClassName;
		this.windowSize = windowSize;
		this.increaseBase = increaseBase;
		this.timespan = timespan;
		
		loggers = new ArrayList<Logger>();
	}
	
			
	public void StartPooling() {
		/**Here we create all the threads starting from properties and we start them*/
				PrintConfiguration();

				/**Simulating the growth*/
				switch(growthMode){
					case Exponential: ExponentialGrowth(upperBound);break;
					case Linear: LinearGrowth(upperBound); break;
					case Windowed : {
						if(windowSize >0)
						WindowedGrowth(windowSize, upperBound);
					} break;
				}
	}

	
	private void PrintConfiguration() {
		/**Now we test the properties read went well*/
		System.out.println("-- CONFIG PARAMETERS --");
		System.out.println("Upper bound: "+ upperBound +"\nGrowth Mode: "+growthMode+"\nClass: "+concreteClassName);
		System.out.println("Timespan: "+ timespan +"\nIncrease Base: "+increaseBase+"\nWindow Size: "+windowSize);
		
		System.out.println("-- END PARAMETERS --\n\n");
	}


	private void StartClient(int threadId) {
		/**In this function we put the logic so that other functions are independent*/
		Inform("{MAIN THREAD} metto in esecuzione "+threadId);
		/**We want to use reflection, so here we do instantiate the .class
		 * set in the property file.*/
		if(concreteClassName != null){
			try {
						
				ISimulatedClass instance = (ISimulatedClass) Class.forName(concreteClassName).newInstance();
				instance.setId(threadId+"");
				instance.start();
				
			} catch (ClassNotFoundException e) {
				System.out.println("Class path is not correct.");
			} catch (InstantiationException e) {
				System.out.println("Instanciation not good.");
			} catch (IllegalAccessException e) {
				System.out.println("Illegal access to file.");
			}
		}
		
	}

	
	private void LinearGrowth(int upperBound) {
		for(int i= 0; i < upperBound ; i++){
			StartClient(i);

			Log(i);
			Inform("Numero thread: "+i);
			PauseBatch();
		}

		SignalEndSpawn();
		
	}

	private void ExponentialGrowth(int upperBound){
		int threadNumber = 0;
		for(int i=0; threadNumber < upperBound; i++){
			Inform("Main Thread - Thread Batch #"+i);
			int previousThreadNumber = threadNumber;
			threadNumber = (int) Math.pow(increaseBase, i);
			/**DEBUG
			System.out.println(threadNumber+" "+(threadNumber-previousThreadNumber));*/
			for(int j=0; j<(threadNumber-previousThreadNumber) && (previousThreadNumber+j)< upperBound; j++){
				int actualThread = (previousThreadNumber + j);
				StartClient(actualThread);
				Log(actualThread);
				Inform("Numero thread: "+actualThread);
			}

			
			PauseBatch();
		}

		SignalEndSpawn();
	}
	
	private void WindowedGrowth(int windowSize, int upperBound){
		int threadNumber = 0;
		for(int i=0; threadNumber < upperBound; i++ ){
			for(int j=0 ; threadNumber< upperBound && j<windowSize; j++){
				StartClient(threadNumber);
				threadNumber++;
				Log(threadNumber);
				Inform("Numero thread: "+threadNumber);
			}			
			PauseBatch();
		}

		SignalEndSpawn();
	}
	
	private void PauseBatch(){
		try {
			Thread.sleep(timespan);
		} catch (InterruptedException e) {
			System.out.println("Could not pause between one batch and the other!");
		}
	}
	
	private void SignalEndSpawn(){
		/**Here we can add optional behaviours*/
		Inform("Finished spawning clients.");
	}
	
	public void addLogger(Logger toAdd){
		loggers.add(toAdd);
	}
	
	private void Log(int value){
		for(Logger l : loggers){
			l.LogData(value);
		}
	}
	
	
	private void Log(String value){
		for(Logger l : loggers){
			l.LogData(value);
		}
	}
	
	private void Inform(String value){
		for(Logger l: loggers){
			l.InformationalMessage(value);
		}
	}
}


