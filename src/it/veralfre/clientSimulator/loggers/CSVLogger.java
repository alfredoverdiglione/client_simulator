package it.veralfre.clientSimulator.loggers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import it.veralfre.interfaces.Logger;

public class CSVLogger implements Logger {
	private CSVPrinter printer;
	private String fileName;
	private CSVFormat formatter;
	
	private SimpleDateFormat dateFormatter;
	
	public CSVLogger(String fileName){
		
		dateFormatter = new SimpleDateFormat("HH:mm:ss ");
		
		this.fileName = fileName;
		formatter = CSVFormat.RFC4180;
		try {
			printer = new CSVPrinter(new PrintWriter(fileName), formatter);
			
			List<String[]> headers = new ArrayList<String[]>();
			
			/**We need to print the headers of out columns*/
			headers.add(new String[]{"Time","Thread_Number"});
			
			printer.printRecords(headers);
			
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
		} catch (IOException e) {
			System.out.println("Error in IO!");
		}
		
		
	}
	
	@Override
	public void LogData(int value) {
		List<String[]> datas = new ArrayList<String[]>();
		datas.add(new String[]{""+dateFormatter.format(new Date()),""+value});
		if(printer!=null)
			try {
				printer.printRecords(datas);
				printer.flush();
			} catch (IOException e) {
				System.out.println("Error in IO!");
			}

	}

	@Override
	public void LogData(String value) {
		List<String[]> datas = new ArrayList<String[]>();
		datas.add(new String[]{""+dateFormatter.format(new Date()),""+value});
		if(printer!=null)
			try {
				printer.printRecords(datas);
				printer.flush();
			} catch (IOException e) {
				System.out.println("Error in IO!");
			}
	}

	@Override
	public void InformationalMessage(String value) {
		/**This doesn't actually do anything*/
		if(printer!=null)
			try {
				printer.printComment(value);
				printer.flush();
			} catch (IOException e) {
				System.out.println("Error in IO!");
			}
	}

}
