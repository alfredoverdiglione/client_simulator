package it.veralfre.clientSimulator.loggers;

import java.util.Date;

import it.veralfre.interfaces.Logger;

public class TextLogger implements Logger {

	@Override
	public void LogData(int value) {
		System.out.println("["+new Date()+"]Value: "+value);

	}

	@Override
	public void LogData(String value) {
		System.out.println("["+new Date()+"]Value: "+value);
	}

	@Override
	public void InformationalMessage(String value) {
		System.out.println("["+new Date()+"]Information: "+value);
	}

}
