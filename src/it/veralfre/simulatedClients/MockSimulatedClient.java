package it.veralfre.simulatedClients;

import java.util.Date;

import it.veralfre.interfaces.ISimulatedClass;

public class MockSimulatedClient extends ISimulatedClass {
	/* extendedName = "it.veralfre.simulatedClients.MockSimulatedClient" */
	
	public MockSimulatedClient() {
		super();
	}
	
	@Override
	public void run(){
		/**This mock class just sits and print every now and then time.*/
		while(true){
			System.out.println("Thread "+this.instanceId+" printing at "+(new Date()));
			try {
				Thread.sleep(50*1000);
			} catch (InterruptedException e) {
				System.out.println("Error in sleeping!");
			}
		}
	}
	
}
