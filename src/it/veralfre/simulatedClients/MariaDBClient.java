package it.veralfre.simulatedClients;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import it.veralfre.interfaces.*;

public class MariaDBClient extends ISimulatedClass {
	/* extendedName = "it.veralfre.simulatedClients.MariaDBClient" */
	private Connection c;
	private static final String hostAddress ="jdbc:mariadb://192.168.1.106:3306/data";
	private ArrayList<String> queries;
	private Random generator;
	
	private static final String queryDownloadText = "SELECT Body "
												  + "FROM Queries ";
	private static final int period = 2;
	
	
	public MariaDBClient(){
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			
			c = DriverManager.getConnection(hostAddress,"monitoring","monitor");
			/*Now we download custom queries from Database*/
			queries = new ArrayList<String>();
			downloadQueries();
			generator = new Random();
			
			
		} catch (ClassNotFoundException e) {
			System.out.println("Eccezione nell'istanza della classe Driver!"+e.getStackTrace());
		} catch (SQLException e) {
			System.out.println("Eccezione nella connessione SQL!"+e.getStackTrace());
		} 
	}
	
	private void downloadQueries() {
		try {
			
			Statement query = c.createStatement();
			query.execute("/*"+new Date()+"*/ "+queryDownloadText);
			
			ResultSet rs = query.getResultSet();
			while(rs.next()){
				queries.add(rs.getString("Body"));
			}
			
			query.close();
			Thread.sleep(period);
	
		} catch (SQLException e) {
				System.out.println("Eccezione nell'effettuare la  richiesta Sql!");
				e.printStackTrace();
		} catch (InterruptedException e) {
				e.printStackTrace();
		}

		
	}

	@Override
	public void run(){
		while(true){
			
				
				
				/*Continuosly launchs request on a random query*/
				doQuery();
				
			
		}
	}

	private void doQuery() {
		try {
				Statement query = c.createStatement();
				query.execute("/*"+new Date()+"*/ "+queries.get(generator.nextInt(queries.size())));
				query.close();
				Thread.sleep(period);
		
		} catch (SQLException e) {
			System.out.println("Eccezione nell'effettuare la  richiesta Sql!");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
