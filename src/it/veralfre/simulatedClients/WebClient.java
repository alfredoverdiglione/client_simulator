package it.veralfre.simulatedClients;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

import it.veralfre.interfaces.ISimulatedClass;

public class WebClient extends ISimulatedClass {
	/* extendedName = "it.veralfre.simulatedClients.WebClient" */
	
	/* Put here host name to reach and test*/
	private String host = "192.168.1.105";
	/* Put here port number*/
	private String port = "8080";
	/*Not to ddos the website we decide a 2 seconds cooldown*/
	private int interval = 500;
	
	public WebClient(){
		Unirest.setTimeouts(3000, 3000);
	}
	
	@Override
	public void run(){
		while(true){
			
			//DEBUG System.out.println("Provo a inviare la richiesta..");
			GetRequest response = Unirest.get("http://"+host+":"+port);
			//DEBUG System.out.println("Richiesta eseguita..");
			/*DEBUG
			 * */
			try {
			 		 // WITHOUT the as* method the request does not trigger, example given System.out.print(response.asString().getBody());
			 		 response.asString();
				} catch (UnirestException e1) {
				System.out.println("Errore nello stampare..");
				e1.printStackTrace();
			   }
			
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				System.out.println("Errore nel tentativo di stoppare il Thread..");
				e.printStackTrace();
			}
		}
	}
}
